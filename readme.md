# Firmware for the [Ergotron keyboard](https://benjamin.vanryseghem.com/2015/10/23/Ergotron.html)


This is a fork of [ergodox firmware](https://github.com/benblazak/ergodox-firmware) tailor-made
for my own keyboard inspired by the Maltron keyboard.

For more info, please read [the original README](old-readme.md) file.

The code is base on the [partial-rewrite](https://github.com/benblazak/ergodox-firmware/tree/partial-rewrite) branch.

-------------------------------------------------------------------------------

Copyright &copy; 2015-2016 Benjamin Van Ryseghem <benjamin.vanryseghem@gmail.com>

Distributed under a MIT License (MIT) (see "license.md")
